/* interfaces
database::create_database(database_name: String)
updates config.env with the database name
creates a new database file based on the database_name

database::default_database_exists -> bool
checks if the config.env file exists, whether there is a DATABASE_URL field in that
file, and whether the database file that DATABASE_URL refers to exists and can
be read.
*/
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

pub mod models;
pub mod schema;

const DATABASE_NAME : &str = "School Database";

pub fn create_database() {
    // create an empty database
    SqliteConnection::establish(&DATABASE_NAME)
    .expect(&format!("Error connecting to {}", DATABASE_NAME));
}
