use diesel::prelude::*;
use crate::database::schema::students;

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::database::schema::students)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct Students {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub phone_number: String,
}

#[derive(Insertable)]
#[diesel(table_name = students)]
pub struct NewStudent<'a> {
    pub id: &'a i32,
    pub first_name: &'a str,
    pub last_name: &'a str,
    pub phone_number: &'a str,
}