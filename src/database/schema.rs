// @generated automatically by Diesel CLI.

diesel::table! {
    students (id) {
        id -> Int4,
        first_name -> Text,
        last_name -> Text,
        phone_number -> Text,
    }
}