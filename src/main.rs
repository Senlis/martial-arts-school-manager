mod ui;
use ui::TextUI;

mod database;
// this struct is displayed when the application runs for the first time
// or if the config file is deleted
// to initate the UI, create an Application object,
// call the default constructor and show method in a "connect_startup" call

fn main() {
    let mut text_ui: TextUI = TextUI::new();

    text_ui.start_ui();
}
