// @generated automatically by Diesel CLI.

diesel::table! {
    students (ID) {
        ID -> Integer,
        FirstName -> Nullable<Text>,
        LastName -> Nullable<Text>,
        PhoneNumber -> Nullable<Text>,
    }
}
