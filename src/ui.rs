/* interfaces
 TextUI::new()
 creates a new instance of the user interface

 TextUI::start_ui()
 starts the user interface and, essentially, takes control of the program
*/

use crate::database;
use std::io::{self};
use std::process::Command;
pub struct TextUI {
    message: String,
}

impl TextUI {
    pub fn new() -> Self {
        let message: String = ("").to_string();

        TextUI { message }
    }

    pub fn start_ui(&mut self) {
        self.clear_terminal();

        //display introduction
        println!("Martial Arts School Manager");
        println!("Developmental version");
        println!("By Richard Romick");
        println!("This program is licensed under the GPTv3 license");
        println!("=====================================================");
        println!("Press Enter to Continue:");

        // wait for the user to press enter and then continue
        let mut input_text: String = String::new();
        let _ = io::stdin().read_line(&mut input_text).expect("An unknown error has occured.  Continuing.");

        database::create_database();

        self.main_menu();
    }

    fn clear_terminal(&self) {
        if cfg!(target_os = "windows") {
            Command::new("cmd")
                    .args(&["/C", "cls"])
                    .status()
                    .unwrap();
        } else {
            Command::new("clear")
                    .status()
                    .unwrap();
        }
    }
    
    fn clear_message(&mut self) {
        self.message = ("").to_string();
    }

    fn main_menu(&mut self) {
        let mut choice: String = ("").to_string();

        while choice != "x" {
            //display menu header
            self.clear_terminal();

            if self.message != "" {
                println!("{}", self.message);
                self.clear_message();
            }

            println!("Main Menu");
            println!("Options: [s]tudents, e[x]it");

            //get choice from user
            let mut input_text: String = String::new();
            match io::stdin().read_line(&mut input_text) {
                Ok(_) => {
                    choice = input_text.trim().to_string();
                },
                Err(error) => {
                    // An error occurred
                    println!("Failed to read line: {}", error);
                }
            }
            
            if choice == "s" {
                let exit : bool = self.students_menu();
                if exit {return}
            }
        }
    }

    /*
    students_menu method
    returns true if user wants to exit program
    returns false if user wants to go back to main menu
     */
    fn students_menu(&mut self) -> bool {
        let mut choice: String = ("").to_string();

        while choice != "m" {
            //display menu header
            self.clear_terminal();

            if self.message != "" {
                println!("{}", self.message);
                self.clear_message();
            }

            println!("Students Menu");
            println!("List of students goes here");
            println!("Options: [a]dd student, [m]ain menu, e[x]it");

            //get choice from user
            let mut input_text: String = String::new();
            match io::stdin().read_line(&mut input_text) {
                Ok(_) => {
                    choice = input_text.trim().to_string();
                },
                Err(error) => {
                    // An error occurred
                    println!("Failed to read line: {}", error);
                }
            }

            if choice == "a" { //add new student option
                self.add_new_student();
            } else if choice == "x" { //exit program option
                return true;
            } else if choice != "m" { //if no valid input was recevied
                self.message = "invalid menu option".to_string();
            }
            
        }
        return false;
    }

    fn add_new_student(&mut self) {
        self.message = "add student menu ran".to_string();
    }
}